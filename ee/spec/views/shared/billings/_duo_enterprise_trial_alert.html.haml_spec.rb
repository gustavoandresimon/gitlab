# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'shared/billings/_duo_enterprise_trial_alert.html.haml', :saas, feature_category: :acquisition do
  let(:group) { build(:group, id: non_existing_record_id) }

  before do
    allow(view).to receive(:current_user)
  end

  def render
    super(partial: 'shared/billings/duo_enterprise_trial_alert', locals: { namespace: group })
  end

  context 'when ultimate plan' do
    before do
      build(:gitlab_subscription, :ultimate, namespace: group)
    end

    it 'contains the correct text' do
      render

      expect(rendered).to have_content('Introducing GitLab Duo Enterprise')

      expect(rendered).to have_content(
        'Start a GitLab Duo Enterprise trial to try all end-to-end AI ' \
          'capabilities from GitLab. You can try it for free for 60 days, ' \
          'no credit card required.'
      )
    end

    it 'contains the primary action' do
      render

      expect(rendered).to have_link(
        'Start a free GitLab Duo Enterprise Trial',
        href: new_trials_duo_enterprise_path(namespace_id: group.id)
      )

      expect(rendered)
        .to have_internal_tracking(event: 'click_duo_enterprise_trial_billing_page', label: 'duo_enterprise_trial')
    end

    it 'contains the hand raise lead selector' do
      render

      expect(rendered).to have_selector('.js-hand-raise-lead-trigger')
    end
  end
end
