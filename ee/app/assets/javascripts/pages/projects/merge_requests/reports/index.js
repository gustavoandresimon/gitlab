import { initMrPage } from '~/pages/projects/merge_requests/page';
import initReportsApp from 'ee/merge_requests/reports';

initMrPage();
initReportsApp();
